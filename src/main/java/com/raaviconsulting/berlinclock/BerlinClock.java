package com.raaviconsulting.berlinclock;

import org.apache.commons.lang3.StringUtils;

/**
 * Create a representation of the Berlin Clock for a given time (hh::mm:ss).
 * 
 * @author Aamir Yaseen
 */
public class BerlinClock {
    
    //input format to validate time for Berlin clock.
    private final String clockPattern = "([01]?[0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9]";

    //respresent 3 possible states of second's lamp
    private static final String YELLOW = "Y";
    private static final String RED = "R";
    private static final String OFF = "O";
    
    private static final String SPCACE = " ";

    private int hours, minutes, seconds;

    /**
     * 
     * @return time in Berlin clock format
     */
    public String getTime() {
        StringBuilder s = new StringBuilder();
        s.append(getSeconds());
        s.append(getHours());
        s.append(getMinutes());
        return s.toString();
    }

    /**
     * Validates and set local variables to represent given time
     * @param time 
     */
    public void setTime(String time) {
        if (time.matches(clockPattern)) {
            String[] pieces = time.split(":");
            setHours(pieces[0]);
            setMinutes(pieces[1]);
            setSeconds(pieces[2]);
        } else {
            throw new IllegalArgumentException("Invalid time format, time must be in 'hh::mm:ss' format.");
        }
    }

    public String getSeconds() {
        if (seconds % 2 == 0) {
            return YELLOW + SPCACE;
        } else {
            return OFF + SPCACE;
        }
    }

    public String getHours() {
        int quotient = hours / 5;
        int remainder = hours % 5;
        StringBuilder s = new StringBuilder();
        s.append(rowOfFour(quotient));
        s.append(SPCACE);
        s.append(rowOfFour(remainder));
        s.append(SPCACE);
        return s.toString();
    }

    public String getMinutes() {
        int quotient = minutes / 5;
        int remainder = minutes % 5;
        StringBuilder s = new StringBuilder();
        s.append(rowOfEleven(quotient));
        s.append(SPCACE);
        s.append(rowOfFour(remainder).replace(RED, YELLOW));
        return s.toString();
    }

    private void setSeconds(String s) {
        int i = Integer.parseInt(s);
        if (i >= 0 && i <= 59) {
            seconds = i;
        } else {
            throw new IllegalArgumentException(
                    "seconds must be between 0 and 59"
            );
        }
    }

    private void setMinutes(String s) {
        int i = Integer.parseInt(s);
        if (i >= 0 && i <= 59) {
            minutes = i;
        } else {
            throw new IllegalArgumentException(
                    "minutes must be between 0 and 59"
            );
        }
    }

    private void setHours(String s) {
        int i = Integer.parseInt(s);
        if (i >= 0 && i <= 24) {
            hours = i;
        } else {
            throw new IllegalArgumentException(
                    "hours must be between 0 and 24"
            );
        }
    }

    private String rowOfFour(int value) {
        String[] off = {OFF, OFF, OFF, OFF};
        for (int i = 0; i < value; i++) {
            off[i] = RED;
        }
        return StringUtils.join(off, "");
    }

    private String rowOfEleven(int value) {
        String[] off = {OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF};
        for (int i = 0; i < value; i++) {
            if ((i + 1) % 3 == 0) {
                off[i] = RED;
            } else {
                off[i] = YELLOW;
            }
        }
        return StringUtils.join(off, "");
    }
}
